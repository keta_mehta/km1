>>> s1='hello'
>>> s2="world"
>>> s3='''hello
... world
... 1234
... '''
>>> print(s1)
hello
>>> print(s2)
world
>>> print(s3)
hello
world
1234

>>> s3
'hello\nworld\n1234\n'
>>> 

